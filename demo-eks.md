## Create EKS cluster

- https://eksctl.io/
- https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster

```shell
# Create EKS
eksctl create cluster --name=gitlab --region=eu-central-1 --node-type t3.medium
# Verify
kubectl get no

```

## Create GitLab project

Create project in GitLab: https://gitlab.com/cinqict-devops

+ create project > from template > Ruby

## Connect to existing cluster

Operations > Kubernetes > existing cluster

## Gather data for GitLab connection

```shell
# API URL
kubectl cluster-info | grep 'Kubernetes' | awk '/http/ {print $NF}'

# CA Certificate
# First: Get secret name
SECRET_NAME_CA=$(kubectl get secret -o 'jsonpath={.items[*].metadata.name}')
# Then: Get CA cert
kubectl get secret $SECRET_NAME_CA -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

# Service token
# First: Create service account
cat << EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
EOF
# Second: Get secret name
SECRET_NAME_CA=$(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') 
# Then: Get service token
kubectl -n kube-system get secret $SECRET_NAME_CA -o jsonpath="{['data']['token']}" | base64 --decode

```

## Install apps

- Ingress
  
- Cert Manager
- Prometheus
- GitLab Runner

## Show ELB and connect Route 53

[ELB](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#LoadBalancers:sort=loadBalancerName) is created by the `ingress` app of GitLab.

Connect [Route 53](https://console.aws.amazon.com/route53/v2/home#Dashboard) to the ELB.

Hosted zones > c3s.io > Create record

- Record policy: Simple routing
- Record name: *.c3s.io
- Record type: CNAME - Routes traffic to another domain name and to some AWS resources
- Value: <copy the ELB domain name here>

## GitLab: configure base domain

`c3s.io`

## Clean up

```shell

eksctl delete cluster --name=gitlab --region=eu-central-1

```

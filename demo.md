
# GitLab Auto DevOps - Demo

## Prereq AWS

- [ ] [Create and connect EKS cluster](./demo-eks.md).

## Prereq GitLab

- [ ] Delete demo_1 repo

- [ ] Connect clean k8s cluster `app.gitlab.com/app=cinqict-devops-demo-1`

- [ ] Create repo's

```shell
cd ~/code/cinq/spring-boot-helloworld
#rm -rf .git
git remote set-url origin git@gitlab.com:cinqict-devops/demo_1.git
git push
git remote set-url origin git@gitlab.com:cinqict-devops/demo_2.git
git push
git remote set-url origin git@gitlab.com:cinqict-devops/demo_3.git
git push

```

- [ ] Create project demo_1
  - No Auto DevOps

- [ ] Create project demo_2
  - Auto DevOps enabled

- [ ] Create project demo_3
  - Auto DevOps enabled
  - create MR

## Demo Script

Auto DevOps + k8s

### Auto DevOps

- Show project demo-1
- Show/note:
  - Show: How to connect your k8s cluster
  - Show: install apps
  - Show: base domain
- Eanble Auto DevOps
- Show repo
  - no Dockerfile
  - no CI/CD config
  - just only a spring boot app

## Show pipeline steps (Demo_2 is backup)

- Build
  - Buildpacks (Heroku, Cloud Native)
    - detect language
    - Dockerfile
    - Helm chart

- Test
  - Code Quality
    - Static code analysis
    - based on CodeClimate
    - support same languages as buildpacks (more or less)
  - Container scanning
    - Based on Clair from Quay.
  - Dependency Scanning
    - based on Gemnasium (acquired by GitLab)
    - scan for vulnerabilities in your dependecies
    -> it's a security scan, not a versioning scan!
  - License Scanning
    - Based on License Finder
    - you can create a license policy
    - Security & Compliance > License Compliance
  - Secret Detection
    - based on GitLeaks
    - scans commits on secrets
    - can also scan full history
  - SAST static application security testing
    - uses different SAST tooling per language
  - Test (also based on builpacks)
    - detect language
    - runs YOUR tests

- Review
  - works only with k8s
  - Deploy app in review env
  - for each branch

- DAST
  - Dynamic Application Security Testing
  - Uses OWASP ZAP
    - popular web app security scanner

- Performance
  - uses sitespeed.io
  - a browsers performance tool
  - default browsers: Chrome and Firefox

- Remarks:
  - all json reports are available
  - all tools are either open-source or GitLab tooling
  - all steps can be configured using a gitlab YAML file `.gitlab-ci.yml`
  - all code for each pipeline step is opensource
  - show registry (Packages & Registries)

- Production
  - Show environments and link to prod-env

## Demo_1 Create MR

- Make change
- Make MR
- Show new pipeline
- Show new steps

Switch to MR of existing project demo-3

- Show pipeline again
- Show steps
- Show environments and link to branch-env

Peak on k8s cluster

- `k get ns`
- `k get po -A`

Conclusion:

- (all either open-source or GitLab tooling)
- GitLab is not really a all-in-one tool,
  - it just integrated a compleet toolchain for you.
- k8s is not required, but it works really great
  Recall: (go to the k8s apps page again?)
  - base domain (ingress)
  - cert manager
  - gitlab runner

-> Final slide








## Reading / References

- All pipeline steps: https://docs.gitlab.com/ee/topics/autodevops/stages.html
- Independent blog on auto devops: https://medium.com/@wijnandtop/gitlabs-auto-devops-java-spring-boot-with-quality-control-to-production-in-minutes-using-7afdbc859b9a
- Spring boot project: https://github.com/JavaExamples/spring-boot-helloworld
- only 7 months old demo: https://www.youtube.com/watch?v=7q9Y1Cv-ib0
- Old Gitlab demo:
  - https://about.gitlab.com/blog/2018/08/10/gitlab-auto-devops-in-action/
  - https://www.youtube.com/watch?v=nMAgP4WIcno&feature=youtu.be&mkt_tok=eyJpIjoiTkRObE9URXhNMlEzTmpWbSIsInQiOiJaYkVDMjY0Q2RwSXQ0alV3V2xsRVBTVnlBVXdFXC9VWFd1Q3hIK25lbVpCWGVtdDZZMEVLWGRoRSt0UVBaOXVHMDBPeEp6RWUrb3VYdmtzYzZWejJOc1hQRW5tVUJqTm9HTTNUNGV4dG5zM3Z2d2RaclVFdmFCR1NaMjA3dnJyUUsifQ%3D%3D
- auto devops explained + 45min video: https://about.gitlab.com/blog/2019/10/07/auto-devops-explained/

- Customize pipelines: https://docs.gitlab.com/ee/topics/autodevops/customize.html











